package id.co.trading.utils;

public class StringUtils {

    public static boolean isValidInt(String token) {
        boolean isInt = true;
        try {
            Integer.parseInt(token);
        } catch (Exception e) {
            isInt = false;
        }
        return isInt;
    }

    public static boolean isValidDouble(String token) {
        boolean isDouble = true;
        try {
            Double.parseDouble(token);
        } catch (Exception e) {
            isDouble = false;
        }
        return isDouble;
    }

}
