package id.co.trading.utils;

public enum IgnoredVariables {
    QUESTION_MARK("?"),
    CREDITS("credits"),
    IS("is");

    private String variable;

    IgnoredVariables(String variable) {
        this.variable = variable;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public static boolean isIgnoredVariables(String check) {

        for(IgnoredVariables variable: IgnoredVariables.values()) {
            if (variable.getVariable().equals(check)) {
                return true;
            }
        }

        return false;
    }
}
