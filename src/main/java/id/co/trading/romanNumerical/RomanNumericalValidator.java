package id.co.trading.romanNumerical;

import org.apache.log4j.Logger;

import static id.co.trading.romanNumerical.RomanNumerical.*;

public class RomanNumericalValidator implements Validator {

    Logger logger = Logger.getLogger(RomanNumericalValidator.class);

    private String validateString;

    public RomanNumericalValidator(String validateString) {
        this.validateString = validateString;
    }

    public boolean validate() {

        if (validateString == null || validateString.isEmpty()) {
            return false;
        }

        RomanNumerical currentRomanNumerical = null;
        int repeatedVariable = 0;
        for (int i = 0; i < validateString.length(); i++) {
            char stringChar = validateString.charAt(i);

            if (RomanNumerical.fromString(stringChar) == null) {
                return false;
            }

            if (currentRomanNumerical != null) {

                if (currentRomanNumerical != RomanNumerical.fromString(stringChar)) {
                    repeatedVariable = 0;
                }
                else {
                    if (repeatedVariable == 3) {
                        return false;
                    }

                    repeatedVariable++;
                }
            }

            currentRomanNumerical = RomanNumerical.fromString(stringChar);

            if ((i+1) < validateString.length()) {

                RomanNumerical nextRomanNumerical = RomanNumerical.fromString(validateString.charAt(i+1));
                boolean validatePredecessor = validatePredecessor(currentRomanNumerical, nextRomanNumerical);

                if (!validatePredecessor) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean validatePredecessor(RomanNumerical current, RomanNumerical next) {

        if (current.getIntegerValue() >= next.getIntegerValue()) {
            return true;
        }

        switch(current) {
            case I: if (next != V && next != X)
                return false;
            break;
            case X:
                logger.info("X " + (next != L && next != C));
                if (next != L && next != C)
                return false;
            break;
            case C:
                logger.info("after X ");
                if (next != D && next != M)
                return false;
            break;
        }

        return true;
    }

}
