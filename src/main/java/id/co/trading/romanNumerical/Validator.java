package id.co.trading.romanNumerical;

public interface Validator {
    boolean validate();
}
