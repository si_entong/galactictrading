package id.co.trading.romanNumerical;

import org.apache.log4j.Logger;

public class RomanNumericalConversion {

    private static Logger logger = Logger.getLogger(RomanNumericalConversion.class);

    public static Integer convert(String input) {

        if (!new RomanNumericalValidator(input).validate()) {
            logger.warn(input + " is not a valid roman numerical");
            return null;
        }

        int integerValue = 0;
        int tempValue = 0;
        for(int index = 0; index < input.length(); index++) {
            RomanNumerical firstRomanNumerical = RomanNumerical.fromString(input.charAt(index));
            tempValue = firstRomanNumerical.getIntegerValue();
            if ((index+1) < input.length()) {
                RomanNumerical nextRomanNumerical = RomanNumerical.fromString(input.charAt(index+1));
                if (nextRomanNumerical.getIntegerValue() > firstRomanNumerical.getIntegerValue()) {
                    tempValue = nextRomanNumerical.getIntegerValue() - firstRomanNumerical.getIntegerValue();
                    index++;
                }
            }
            integerValue += tempValue;
        }

        return integerValue;
    }


}
