package id.co.trading.romanNumerical;

public enum RomanNumerical {
    I('I', 1),
    V('V', 5),
    X('X', 10),
    L('L', 50),
    C('C', 100),
    D('D', 500),
    M('M', 1000);

    private char numeric;
    private int integerValue;

    RomanNumerical(char numeric, int integerValue){
        this.numeric = numeric;
        this.integerValue = integerValue;
    };

    public char getNumeric() {
        return numeric;
    }

    public void setNumeric(char numeric) {
        this.numeric = numeric;
    }

    public int getIntegerValue() {
        return integerValue;
    }

    public void setIntegerValue(int integerValue) {
        this.integerValue = integerValue;
    }

    public static RomanNumerical fromString(char value) {

        for(RomanNumerical romanNumerical: RomanNumerical.values()){
            if (romanNumerical.getNumeric() == value){
                return romanNumerical;
            }
        }

        return null;
    }
}
