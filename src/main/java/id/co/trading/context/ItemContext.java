package id.co.trading.context;

import id.co.trading.romanNumerical.RomanNumerical;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemContext {

    private List<String> inputStrings;
    private List<String> outputStrings;
    private Map<String, RomanNumerical> romanNumericalMap;
    private Map<String, Double> itemValues;

    public ItemContext() {
        super();
        this.inputStrings = new ArrayList<>();
        this.outputStrings = new ArrayList<>();
        this.romanNumericalMap = new HashMap<>();
        this.itemValues = new HashMap<>();
    }

    public Map<String, RomanNumerical> getRomanNumericalMap() {
        return romanNumericalMap;
    }

    public ItemContext setRomanNumericalMap(Map<String, RomanNumerical> romanNumericalMap) {
        this.romanNumericalMap = romanNumericalMap;
        return this;
    }

    public Map<String, Double> getItemValues() {
        return itemValues;
    }

    public ItemContext setItemValues(Map<String, Double> itemValues) {
        this.itemValues = itemValues;
        return this;
    }

    public List<String> getInputStrings() {
        return inputStrings;
    }

    public ItemContext setInputStrings(List<String> inputStrings) {
        this.inputStrings = inputStrings;
        return this;
    }

    public List<String> getOutputStrings() {
        return outputStrings;
    }

    public ItemContext setOutputStrings(List<String> outputStrings) {
        this.outputStrings = outputStrings;
        return this;
    }
}
