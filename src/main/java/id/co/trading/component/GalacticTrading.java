package id.co.trading.component;

import id.co.trading.context.ItemContext;
import id.co.trading.romanNumerical.RomanNumerical;
import id.co.trading.romanNumerical.RomanNumericalConversion;
import id.co.trading.utils.IgnoredVariables;
import id.co.trading.utils.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

@Service
public class GalacticTrading {

    private static final String PREFIX_ITEM_SOLD_QUESTION = "how many credits is ";
    private static final String PREFIX_ROMAN_NUMERICAL_QUESTION = "how much is ";
    private static final String NO_ANSWER_PROVIDED = "I have no idea what you are talking about";
    private static final String FILE_OUTPUT = "output.txt";

    Logger logger = Logger.getLogger(GalacticTrading.class);

    public ItemContext readFile(String fileName) throws Exception   {

        ItemContext itemContext = new ItemContext();

        try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {

            String line;

            while((line = br.readLine()) != null)
            {
                processInput(line, itemContext);
            }

        } catch (IOException e) {
            logger.error("File not found exception " + e);
        }

        writeFile(itemContext.getOutputStrings(), FILE_OUTPUT);
        return  itemContext;
    }

    public void writeFile(List<String> outputStrings, String fileName){

        try(FileWriter fileWriter = new FileWriter(fileName)){
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (String output : outputStrings) {
                logger.info(output);
                writer.write(output);
                writer.newLine();
            }
            writer.close();
        }
        catch (IOException e) {
            logger.error("Error when writing to file " + e);
        }
    }

    public void processInput(String line, ItemContext itemContext) throws Exception{

        if (line == null || line.isEmpty()) {
            return;
        }

        String lineLowercase = line.toLowerCase();
        String[] splitStringFromLine = lineLowercase.split(" ");

        if (lineLowercase.startsWith(PREFIX_ITEM_SOLD_QUESTION)) {
            String romanNumericalString = lineLowercase.substring(PREFIX_ITEM_SOLD_QUESTION.length());
            String[] romanNumericalQuestions = romanNumericalString.split(" ");
            StringBuffer romanNumericalAnswer = new StringBuffer();

            for (String romanNumericalQuestion: romanNumericalQuestions) {

                if (!IgnoredVariables.isIgnoredVariables(romanNumericalQuestion)) {
                    Map<String, RomanNumerical> romanNumericalMap = itemContext.getRomanNumericalMap();
                    RomanNumerical romanNumerical = romanNumericalMap.get(romanNumericalQuestion);

                    if (romanNumerical == null) {

                        Double unitValue = itemContext.getItemValues().get(romanNumericalQuestion);
                        if (unitValue != null) {
                            Integer integerValue = RomanNumericalConversion.convert(romanNumericalAnswer.toString());

                            if (integerValue == null) {
                                itemContext.getOutputStrings().add(NO_ANSWER_PROVIDED);
                                return;
                            }

                            Double result = integerValue * unitValue;
                            String answer = romanNumericalString.replace("?", "is") +
                                    " " + result + " Credits";
                            itemContext.getOutputStrings().add(answer);
                        }

                        return;
                    }

                    romanNumericalAnswer.append(romanNumerical);
                }
            }

            itemContext.getOutputStrings().add(NO_ANSWER_PROVIDED);
            return;
        }

        if (lineLowercase.startsWith(PREFIX_ROMAN_NUMERICAL_QUESTION)) {

            String romanNumericalString = lineLowercase.substring(PREFIX_ROMAN_NUMERICAL_QUESTION.length());
            String[] romanNumericalQuestions = romanNumericalString.split(" ");
            StringBuffer romanNumericalAnswer = new StringBuffer();

            for (String romanNumericalQuestion: romanNumericalQuestions) {

                if (!IgnoredVariables.isIgnoredVariables(romanNumericalQuestion)) {
                    Map<String, RomanNumerical> romanNumericalMap = itemContext.getRomanNumericalMap();
                    RomanNumerical romanNumerical = romanNumericalMap.get(romanNumericalQuestion);

                    if (romanNumerical == null) {
                        logger.error("Unable to find match roman numerical for " + romanNumericalQuestion);
                    }

                    romanNumericalAnswer.append(romanNumerical);
                }
            }

            Integer integerAnswer = RomanNumericalConversion.convert(romanNumericalAnswer.toString());

            if (integerAnswer == null) {
                itemContext.getOutputStrings().add(NO_ANSWER_PROVIDED);
                return;
            }

            String answer = romanNumericalString.replace("?", "is") + " " + integerAnswer;
            itemContext.getOutputStrings().add(answer);
            return;
        }

        if (lineLowercase.contains(IgnoredVariables.IS.getVariable())) {

            String variable = splitStringFromLine[0];
            if (lineLowercase.contains(IgnoredVariables.CREDITS.getVariable())) {
                calculateUnitValue(splitStringFromLine, itemContext);
                return;
            }

            String romanNumericalString = splitStringFromLine[2].toUpperCase();
            if (romanNumericalString.length() > 1) {
                logger.error("Invalid Roman Numerical for line " + lineLowercase);
            }

            Map<String, RomanNumerical> romanNumericalMap = itemContext.getRomanNumericalMap();
            RomanNumerical romanNumerical = RomanNumerical.fromString(romanNumericalString.charAt(0));

            if (romanNumerical == null){
                logger.error("Unidentified Roman Numerical for value " + romanNumericalString);
            }

            romanNumericalMap.put(variable, romanNumerical);

            for(Map.Entry<String, RomanNumerical> pair : romanNumericalMap.entrySet()) {
                logger.info(pair.getKey() + " " + pair.getValue().getNumeric());
            }

            return;
        }

        itemContext.getOutputStrings().add(NO_ANSWER_PROVIDED);
    }

    public void calculateUnitValue(String[] splitStringFromLine, ItemContext itemContext) {

        Map<String, Double> itemValues = itemContext.getItemValues();
        Double unitCredit = new Double(0);
        String unitName = new String();
        StringBuffer unitRomanNumerical = new StringBuffer();

        for (String splitString: splitStringFromLine) {

            if (StringUtils.isValidDouble(splitString)) {
                unitCredit = Double.parseDouble(splitString);
            }
            else if (!IgnoredVariables.isIgnoredVariables(splitString)){
                Map<String, RomanNumerical> romanNumericalMap = itemContext.getRomanNumericalMap();
                RomanNumerical romanNumerical = romanNumericalMap.get(splitString);

                if (romanNumerical == null) {
                    unitName = splitString;
                }
                else {
                    unitRomanNumerical.append(romanNumerical.getNumeric());
                }
            }
        }

        Integer latinNumerical = RomanNumericalConversion.convert(unitRomanNumerical.toString());

        if (latinNumerical == null) {
            itemContext.getOutputStrings().add(NO_ANSWER_PROVIDED);
            return;
        }

        unitCredit = unitCredit/latinNumerical;

        itemValues.put(unitName, unitCredit);
        itemContext.setItemValues(itemValues);
    }
}
